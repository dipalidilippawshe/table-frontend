import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { UserService } from '../user.service';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor(private fb: FormBuilder,private bs: UserService,private router: Router) { }
  angForm: FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";


  createForm() {
    this.angForm = this.fb.group({
      person_name: ['', Validators.required ],
      person_email: ['', Validators.required ,Validators.pattern(this.emailPattern)],
      staff_Id:['',Validators.required],
      person_ContactNo: ['', Validators.required ],
      person_address:[''],
     

    });
  }
  ngOnInit() {
    this.createForm();
  }

  goBack(){
    this.router.navigateByUrl('');
  }
  addUser(name, email, staffId,address,contact) {
   // console.log("INADD BUSINESS");
    let user={
      name: name,
      email:email,
      staffId:staffId,
      address:address,
      contactNo:contact
    }
    this.bs.addUser(user);
    this.reFreshFrom();
    //this.router.navigateByUrl('');

  }
 reFreshFrom(){
  this.angForm.reset();
 }

  


}
