import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import {tap} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  uri = 'http://localhost:8000/user';
  constructor(private http: HttpClient) { }

  private _refreashNeeded$ = new Subject<void>();

  get refreshNeeded$() {
    return this._refreashNeeded$;
  }

  addUser(user){
    this.http.post(`${this.uri}/adduser`, user)
     .pipe(
       tap(()=>{
        this._refreashNeeded$.next();
       })
     )
    .subscribe(res => console.log('Done'));
  }

  getUsers(){
    return this
           .http
           .get(`${this.uri}/getusers`);
  }

  deleteUser(user){
    return this
    .http
    .delete(`${this.uri}/deleteuser/`+user._id);
  }

  updateUser(id,user){
    this.http.put(`${this.uri}/updateuser/`+id,user)
    .pipe(
      tap(()=>{
       this._refreashNeeded$.next();
      })
    )
    .subscribe(res => console.log('Done'));
  }


}
