import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from '../user.service';
import {DataService} from '../data.service';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor(private bs: UserService,private router: Router, private dataService :DataService) {
   
    this.getUsers()
   }
 users=[];
  ngOnInit() {

    this.bs.refreshNeeded$
       .subscribe(()=>{
         this.getUsers();
       })
    this.getUsers();

  }

  private getUsers(){
    this.bs.getUsers()
    .subscribe((data:any) => {
      console.log(data);
      this.users = data.data;
  });
  }

  deleteUser(user){
    this.bs.deleteUser(user).subscribe(data=>{
      console.log("data deleted");
     this.getUsers()
    });
  }

  editUser(user){

    this.dataService.sendData(user)
    //this.router.navigateByUrl('edit');
  }

}
