import { Component } from '@angular/core';
import {DataService} from './data.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Test App';
 currentMode ='';
  constructor(private dataService :DataService)
   {
    this.dataService.ModeIs.subscribe(data=>{
     
      if(data=='edit'){
        this.currentMode =data;
      }else{
        this.currentMode =data;
      }
    });
   }
}
