import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormComponent} from './form/form.component';
import{TableComponent} from './table/table.component';
import{EditComponent} from './edit/edit.component';

import { from } from 'rxjs';

const routes: Routes = [
 // { path: 'create', component:FormComponent},
  {path:'',component:TableComponent},
  {path:'',component:FormComponent}
 // {path:'edit',component:EditComponent}
];
@NgModule({
  imports: [
    CommonModule,
    [ RouterModule.forRoot(routes) ]
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
