import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { UserService } from '../user.service';
import {DataService} from '../data.service';
import { FormGroup,FormBuilder,  Validators } from '@angular/forms';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id:any;
  user: any;
  editForm1:FormGroup;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  constructor(private fb: FormBuilder,private bs: UserService,private router: Router, private dataService :DataService)
   {
    this.editForm();
  }

  ngOnInit() {
    this.dataService.currentMessage.subscribe(data=>{
      console.log("In Edit: ",data);
      this.user = data;
      this.inputData();
      this.id = this.user._id;
    });
  }
  editForm() {
    console.log("@ edit form: ",this.user);
    this.editForm1 = this.fb.group({
      person_name: ['', Validators.required ],
      person_email: ['', Validators.required ,Validators.pattern(this.emailPattern)],
      staff_Id:['',Validators.required],
      person_ContactNo: ['', Validators.required ],
      person_address:[''],
     

    });
  }

  inputData(){
    this.editForm1 = this.fb.group({
      person_name: this.user.name,
      person_email: this.user.email,
      staff_Id:this.user.staffId,
      person_address:this.user.address,
      person_ContactNo:this.user.contactNo
    });
  }


  goBack(){
    this.router.navigateByUrl('');
  }

  updateUser(name, email, staffId,address,contact){
    let user={
      name: name,
      email:email,
      staffId:staffId,
      address:address,
      contactNo:contact
    }
    
    console.log("this.user: ",this.user, "AND Id is: ",this.id);

    this.bs.updateUser(this.id,user);
    this.reFreshFrom();
    this.dataService.changeMode('');
    // .subscribe((data) => {
    //   this.user = data;
    
    //   console.log("this.user: ",this.user, "AND Id is: ",this.id);
    //  }); 
     //this.router.navigateByUrl('');
  }
reFreshFrom(){
  this.editForm1.reset();
 }

}
