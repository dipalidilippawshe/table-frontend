import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private messageSource = new BehaviorSubject('');
   ModeIs = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  constructor() { }

  sendData(user:any){
    this.messageSource.next(user);
    this.ModeIs.next('edit');
  }

  changeMode(data:any){
  
    this.ModeIs.next('Add');
  }

}
